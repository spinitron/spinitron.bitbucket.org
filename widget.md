# Now Playing JavaScript Widget

<!-- This is the container element: -->
<div data-station="kdhx" data-num="8" data-time="1" data-nolinks="1" id="spinitron-nowplaying"></div>

The Now Playing JavaScript Widget puts the most recent spins on your web page without needing an iframe
or use of the SpinPapi API.
A small script fetches markup for recent spins from Spinitron and puts it into a container element on your page.

The widget has three advantages over iframes.

1. It's easier to style because the markup is on your page rather than in a separate page that
the client loads into the iframe. Add style rules for the widget's markup in your page or one of its stylesheets.

2. The widget container automatically assumes the height of its contents (unless you
specify otherwise). It's hard to do that with an iframe so most often they have a specified
static height.

1. It loads faster and with less flicker.

To use the widget, put a container element, a `div` for example, on your page in which the recent
spins should appear, followed by the widget script. Set the container element's ID attribute to
`spinitron-nowplaying`, so the
widget can find it, and add a `data` element to identify your station like this:

```html
<div data-station="wzbc" id="spinitron-nowplaying"></div>
<script src="//spinitron.com/js/npwidget.js"></script>
```

Now you can style the now playing display by adding style rules to the page or one of its stylesheets.
This page is an example – view this page's source or or below for the markup and styles

Other parameters that control the widget are described in the table.


|Parameter | Use|
--- | ---
`station` | Station id, e.g. "wzbc". Mandatory
`num` | Number of spins. Default is "1". Use negative number to reverse the order, e.g. "1", "-5".
`tweets` | Set to "1" to request Twitter share links in the markup.
`time` | Set to "1" to request timestamps at the start of a line. Default is “… spun by …” clause
`nolinks` | Set to "1" to request no links in the response.

The example on this page shows how they are used in the container element.

The style rules and markup used in the example on this page are:

```html
<style>
.widget-box {
    width: 40%;
    float: right;
    margin: 0 0 1em 2em;
    padding: 0.8em 1em 1em 1.5em;
    box-shadow: 0 0 0 1px #CACACA, 0 0 0 4px #EEEEEE;
    font-family: 'Roboto Condensed', sans-serif;
    font-weight: 300;
    line-height: 1.3;
    }

.widget-box h1 {
    font-size: 150%;
    margin-top: .2em;
    }

.widget-box b {
    font-family: Roboto, sans-serif;
    font-weight: 500;
    }

.widget-box p.recentsong {
    margin: 0.5em 0 0;
    }

.widget-box .tweet img {
    vertical-align: -15%;
    }

#spinitron-nowplaying .spunpart,
#spinitron-nowplaying .diskpart,
#spinitron-nowplaying .labelpart {
    display: none;
    }
</style>
```

```html
<div class="widget-box">
    <h1>Recent spins on KZSC</h1>
    <div data-station="kzsc" data-num="8" data-time="1" data-nolinks="1" id="spinitron-nowplaying"></div>
    <script src="//spinitron.com/js/npwidget.js"></script>
</div>
```

###Note

The widget requires a `<script>` element on the page, which some CMS and blog services
block  – wordpress.com does, blogger.com does not. But that doesn't me you can't
use it in WordPress – you probably can if you have your own instance but with the commercial
wordpress.com service you're out of luck.


## Technical notes

The JavaScript source for this widget is available at the URL given above. Feel free to use it
as is, loading it from Spinitron, or copying it to your server, and to modify it however you
please.

There are two security questions you might want to consider. First, loading JavaScript from any
external site, Spinitron included, could be risky if the external site is hijacked. This can be
avoided by hosting the widget script on your own server.

The second consideration is that the widget uses a technique called
[JSONP](http://bob.ippoli.to/archives/2005/12/05/remote-json-jsonp/)
(see also [Wikipedia](http://en.wikipedia.org/wiki/JSONP)) to fetch the latest spins markup from Spinitron.
It works by dynamically adding a `<script>` element to the document head that loads a
JavaScript from Spinitron's `/radio/newestsong.php` service. The JavaScript
nothing but a call to a global function that the widget itself previously declared.
The recent spins markup is in the argument of the function call. The function, when
invoked, replaces the content of the containing div with the markup in the argument.

This JSONP is liable to script injection if Spinitron were to be hijacked in exactly the same way
as the first concern. Unfortunately there isn't a simple solution because the point of
the exercise is to dynamically load data from Spinitron. It isn't possible to use AJAX
instead because browsers would block the call according to the same origin policy.

Before deciding that this risk is unacceptable, consider what is at stake. Does an
attacker stand to gain anything from you or your website's
users by attacking your website in this way? Does running a malicious script within the
context of your page give an attacker access to anything worth having, such as
cookies for an ecommerce user account?

If you prefer not to allow scripts loaded from Spinitron on your site, you could
consider using an iframe or the SpinPapi API instead. However, an iframe is possibly
subject to clickjacking if Spinitron were hacked and malicious content was put in the
iframe. If you are really concerned, the SpinPapi API puts you in complete control
over what is in your web pages.




|Parameter | Explaination|
--- | ---
`ptype` | Set to "s" or "i" to request an HTML snippet, omit for an HTML document.
`callback` | Include to request a JSONP response and specify the JSONP text prefix. Overrides `ptype`, i.e. when `callback` is used, an HTML snippet is returned, not a document.
`click` | In the case of an HTML document response (i.e. `ptype` and `callback` are not set), set to "1" to request that the document reloads when clicked on.
