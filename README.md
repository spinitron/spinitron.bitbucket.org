# spinitron.bitbucket.io

A website using static HTML documents and CSS stylesheets to demonstrate the use of iframe
website integration with Spinitron.

This is live demo. Go to

* [spinitron.bitbucket.io](https://spinitron.bitbucket.io)

to view the pages.

You could, for example, fork this repository and tinker with it to see how it works using Bitbucket
as web host, just as we do.
